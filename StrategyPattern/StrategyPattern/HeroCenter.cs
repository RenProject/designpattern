﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StrategyPattern
{
    public static class HeroCenter
    {
        public static Avenger callHero(SituationType situationType)
        {
            switch(situationType)
            {
                case SituationType.Air:
                    return new IronMan();
                case SituationType.Land:
                    return new CaptainAmerica();
                case SituationType.OuterSpace:
                    return new Thor();
                default:
                    return new IronMan();
            }
        }
    }
}
