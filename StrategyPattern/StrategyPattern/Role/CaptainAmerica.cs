﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StrategyPattern
{
    class CaptainAmerica: Avenger
    {
        public override void Talk()
        {
            Console.WriteLine("我是美國隊長！");
        }
    }
}
