﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StrategyPattern
{
    public class Mjölnir : Weapon.Weapon, IAttack
    {
        public void Attack()
        {
            Console.WriteLine("使用雷神之槌攻擊");
        }
    }
}
