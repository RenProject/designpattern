﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StrategyPattern
{
    class Armor : Weapon.Weapon, IAttack
    {
        public void Attack()
        {
            Console.WriteLine("穿上鋼鐵裝攻擊");
        }
    }
}
