﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demo
{
    public enum EquipType
    {
        Armor = 1,
        Mjölnir = 2,
        Shield = 3
    }
}
