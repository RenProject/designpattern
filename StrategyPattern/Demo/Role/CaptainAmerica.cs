﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demo
{
    public class CaptainAmerica
    {
        public void Talk()
        {
            Console.WriteLine("我是美國隊長！");
        }

        public void equipWeapon(EquipType equipType)
        {
            if (equipType == EquipType.Armor)
            {
                Armor armor = new Armor();
                armor.Attack();
            }
            else if (equipType == EquipType.Mjölnir)
            {
                Mjölnir mjölnir = new Mjölnir();
                mjölnir.Attack();
            }
            else if (equipType == EquipType.Shield)
            {
                Shield shield = new Shield();
                shield.Attack();
            }
        }
    }
}
