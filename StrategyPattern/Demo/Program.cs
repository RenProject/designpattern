﻿using System;

namespace Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            SituationType situationType = SituationType.Air;

            if(situationType == SituationType.Air)
            {
                IronMan ironMan = new IronMan();
                ironMan.Talk();

                ironMan.equipWeapon(EquipType.Armor);
            }
            else if(situationType == SituationType.Land)
            {
                CaptainAmerica captainAmerica = new CaptainAmerica();
                captainAmerica.Talk();

                captainAmerica.equipWeapon(EquipType.Shield);
            }
            else if(situationType == SituationType.OuterSpace)
            {
                Thor thor = new Thor();
                thor.Talk();

                thor.equipWeapon(EquipType.Mjölnir);
            }

            Console.ReadLine();
        }
    }
}
