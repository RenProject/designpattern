﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demo2
{
    public abstract class Weapon
    {
        public abstract void Attack();
    }
}
