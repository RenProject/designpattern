/**
 * 缺點
 * 1. 狀態多時，會產生許多子類別；
 * 2. 實現稍顯複雜；
 * 3. 沒有完全做到『開放/封閉原則』，當有新的狀態加入時，需要修改封裝類；
 */

/**
 * 狀態介面(父類)
 */
class State {

    /**
     * 亮燈
     */
    light(trafficLight) {
        throw new Error("此方法必須重寫!");
    };

    /**
     * 切換號誌
     */
    switchLight(trafficLight) {
        throw new Error("此方法必須重寫!");
    };
}

/**
 * 紅燈類別
 */
class redState extends State {

    /**
     * 亮燈
     */
    light(trafficLight) {

        console.log("紅燈");

        this.switchLight(trafficLight);
    };

    /**
     * 切換號誌
     */
    switchLight(trafficLight) {
        let sec = 5000;
        console.log("等待 " + sec + "毫秒");

        setTimeout(function(){
            //呼叫狀態管理員切換狀態
            trafficLight.switchState(new greenState());
        }, sec);
    };
}

/**
 * 綠燈類別
 */
class greenState extends State {

    /**
     * 亮燈
     */
    light(trafficLight) {

        console.log("綠燈");

        this.switchLight(trafficLight);
    };

    /**
     * 切換號誌
     */
    switchLight(trafficLight) {
        let sec = 3000;
        console.log("等待 " + sec + "毫秒");

        setTimeout(function(){
            //呼叫狀態管理員切換狀態
            trafficLight.switchState(new yellowState());
        }, sec);
    };
}

/**
 * 黃燈類別
 */
class yellowState extends State {

    /**
     * 亮燈
     */
    light(trafficLight) {

        console.log("黃燈");

        this.switchLight(trafficLight);
    };

    /**
     * 切換號誌
     */
    switchLight(trafficLight) {
        let sec = 1000;
        console.log("等待 " + sec + "毫秒");

        setTimeout(function(){
            //呼叫狀態管理員切換狀態
            trafficLight.switchState(new redState());
        }, sec);
    };
}

/**
 * 環境類別(狀態管理者)
 */
class trafficLight {

    constructor(){
        //紀錄目前狀態的變數
        this.currentState = null;
    };

    /**
     * 切換狀態
     * @param State {狀態}
     */
    switchState(State){
        this.currentState = State;

        //執行下一個Loop
        this.currentState.light(this);
    };
}

let go_trafficLight = new trafficLight();
go_trafficLight.switchState(new redState());