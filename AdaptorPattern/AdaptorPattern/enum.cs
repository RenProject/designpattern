﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdaptorPattern
{
    public class @enum
    {
        public enum Signal
        {
            HDMI,
            DVI,
            VGA,
            MiniPort
        }
    }
}
