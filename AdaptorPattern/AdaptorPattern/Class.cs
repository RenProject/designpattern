﻿using System;
using System.Collections.Generic;
using System.Text;
using static AdaptorPattern.@enum;

namespace AdaptorPattern
{
    //一般PC的物件
    public class PC : IDevice
    {
        public Signal Signal { get; set; }
        public PC(Signal signal)
        {
            this.Signal = signal;
        }
    }
    
    //MacBook的物件
    public class MacBook : IDevice
    {
        public Signal Signal { get; set; }
        public MacBook(Signal signal)
        {
            this.Signal = signal;
        }
    }

    //投影機的物件
    public class Projector : IDevice
    {
        public Signal Signal { get; set; }
        public Projector(Signal signal)
        {
            this.Signal = signal;
        }

        public void Display()
        {
            if (this.Signal != Signal.DVI)
            {
                Console.WriteLine("非DVI訊號源, 無法使用投影機");
                return;
            }
            else
            {
                Console.WriteLine("顯示電腦畫面～YA");
            }
        }
    }

    //HDMI轉接器的物件
    public class HDMIAdapter : IAdapter
    {
        public Signal Signal;
        public HDMIAdapter(Signal signal)
        {
            this.Signal = signal;
        }

        public void ConvertSignal()
        {
            //做訊號轉換
            if (this.Signal == Signal.HDMI)
            {
                Console.WriteLine("已轉換成DVI訊號");
                this.Signal = Signal.DVI;
            }
            else
                Console.WriteLine("來源訊號非HDMI, 轉換失敗");
        }
    }

    //MiniPort轉接器的物件
    public class MiniPortAdapter : IAdapter
    {
        public Signal Signal;
        public MiniPortAdapter(Signal signal)
        {
            this.Signal = signal;
        }

        public void ConvertSignal()
        {
            //做訊號轉換
            if (this.Signal == Signal.MiniPort)
            {
                Console.WriteLine("已轉換成DVI訊號");
                this.Signal = Signal.DVI;
            }
            else
                Console.WriteLine("來源訊號非MiniPort, 轉換失敗");
        }
    }

}
