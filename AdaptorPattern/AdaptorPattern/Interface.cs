﻿using System;
using System.Collections.Generic;
using System.Text;
using static AdaptorPattern.@enum;

namespace AdaptorPattern
{
    //設備的介面
    public interface IDevice
    {
        Signal Signal { get; set; }
    }

    //轉接器的介面
    public interface IAdapter
    {
        void ConvertSignal();
    }
}
